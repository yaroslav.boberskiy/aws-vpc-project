# output "jumphost_public_ip" {
#   value = aws_eip.eip_ip_jumphost.public_ip
# }

output "backend_public_ip" {
  value = aws_eip.eip_ip_backend.public_ip
}

# output "jumphost_private_ip" {
#   value = aws_instance.jump_host.private_ip
# }

output "backend_private_ip" {
  value = aws_instance.backend_srv.private_ip
}

output "hyperledger_private_ip" {
  value = aws_instance.hyperledger.private_ip
}

output "private_key_pem" {
  value = tls_private_key.keygen.private_key_pem
}

variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "key_name" {
  type    = string
  default = "pub_key"
}

# variable "jump_host_in_allowed_ports" {
#   description = "Allow ports to jumphost server - Inbound"
#   type        = list
#   default     = ["22"]
# }

variable "backend_in_allowed_ports" {
  description = "Allow ports to Backend server - Inbound"
  type        = list
  default     = ["22"]
}

variable "hyperledger_instance_type" {
  type    = string
  default = "t2.micro"
}

# variable "jumphost_instance_type" {
#   type    = string
#   default = "t2.nano"
# }

variable "backend_instance_type" {
  type    = string
  default = "t2.micro"
}

variable "d_project_vpc_cidr_block" {
  type    = string
  default = "192.168.0.0/16"
}

variable "public_subnet_cidr_block" {
  type    = string
  default = "192.168.60.0/24"
}

variable "private_subnet_cidr_block" {
  type    = string
  default = "192.168.50.0/24"
}

variable "hyperledger_instance_private_ip" {
  type    = string
  default = "192.168.50.11"
}

# variable "jumphost_instance_private_ip" {
#   type    = string
#   default = "192.168.60.5"
# }

variable "backend_instance_private_ip" {
  type    = string
  default = "192.168.60.10"
}

variable "hyperledger_allowed_ports" {
  description = "Allow ports to hyperledger - Inbound"
  type        = list
  default     = ["22", "7051", "9051", "7050", "8054", "9054", "7054"]
}

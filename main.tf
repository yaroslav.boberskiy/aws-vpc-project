// This is simple project for students grades checks with blockcain technology;
// 

provider "aws" {
  region = var.region
}

resource "tls_private_key" "keygen" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.keygen.public_key_openssh
}

resource "aws_vpc" "d_project" {
  cidr_block = var.d_project_vpc_cidr_block

  tags = {
    Name = "D_Project VPC"
  }
}

# resource "aws_security_group" "jumphost_access_sg" {
#   name        = "allow ssh"
#   description = "Allow inbound ports to jump_host"
#   vpc_id      = aws_vpc.d_project.id
#
#   dynamic "ingress" {
#     for_each = var.jump_host_in_allowed_ports
#     content {
#       description = "Access from ANY"
#       from_port   = ingress.value
#       to_port     = ingress.value
#       protocol    = "tcp"
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#   }
#
#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#
#   tags = {
#     Name = "Jump Host IN Security Group"
#   }
# }

resource "aws_security_group" "backend_access_sg" {
  name        = "allow ssh"
  description = "Allow inbound ports from 0.0.0.0/0 (Any)"
  vpc_id      = aws_vpc.d_project.id

  dynamic "ingress" {
    for_each = var.backend_in_allowed_ports
    content {
      description = "Access from ANY"
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Backend IN Security Group"
  }
}

resource "aws_security_group" "hyperledger_access_sg" {
  name        = "allow all from backend_srv"
  description = "Allow inbound ports from backend"
  vpc_id      = aws_vpc.d_project.id

  ingress {
    description     = "Access from Backend_srv"
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.backend_access_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Hyperledger IN Security Group"
  }
}

resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.d_project.id
  cidr_block              = var.public_subnet_cidr_block
  availability_zone       = data.aws_availability_zones.available_azs.names[0]
  map_public_ip_on_launch = true
  depends_on              = [aws_internet_gateway.igw]

  tags = {
    Name = "D_Project Pub Subnet in ${data.aws_availability_zones.available_azs.names[0]}"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_vpc.d_project.id
  cidr_block        = var.private_subnet_cidr_block
  availability_zone = data.aws_availability_zones.available_azs.names[0]

  tags = {
    Name = "D_Project Priv Subnet in ${data.aws_availability_zones.available_azs.names[0]}"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.d_project.id

  tags = {
    Name = "IGW for D_Project VPC"
  }
}

resource "aws_nat_gateway" "nat_gw" {
  depends_on = [aws_internet_gateway.igw]
  allocation_id = aws_eip.eip_ip_nat_gw.id
  subnet_id = aws_subnet.public_subnet.id
}

resource "aws_route_table" "pub_subnet_route_table" {
  vpc_id = aws_vpc.d_project.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  depends_on = [aws_internet_gateway.igw]

  tags = {
    Name = "Public Subnet Route table"
  }
}

resource "aws_route_table" "private_subnet_route_table" {
  vpc_id = aws_vpc.d_project.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gw.id
  }

  depends_on = [aws_internet_gateway.igw]

  tags = {
    Name = "Private Subnet Route table"
  }
}

resource "aws_route_table_association" "public_subnet" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.pub_subnet_route_table.id
}

resource "aws_route_table_association" "private_subnet" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private_subnet_route_table.id
}

# resource "aws_eip" "eip_ip_jumphost" {
#   vpc        = true
#   instance   = aws_instance.jump_host.id
#   depends_on = [aws_internet_gateway.igw]
# }

resource "aws_eip" "eip_ip_backend" {
  vpc        = true
  instance   = aws_instance.backend_srv.id
  depends_on = [aws_internet_gateway.igw]
}

resource "aws_eip" "eip_ip_nat_gw" {
  vpc        = true
  depends_on = [aws_internet_gateway.igw]
}

# resource "aws_instance" "jump_host" {
#   ami             = data.aws_ami.latest_amznlinux.image_id
#   instance_type   = var.jumphost_instance_type
#   private_ip      = var.jumphost_instance_private_ip
#   subnet_id       = aws_subnet.public_subnet.id
#   key_name        = aws_key_pair.generated_key.key_name
#   security_groups = [aws_security_group.jumphost_access_sg.id]
#
#   tags = {
#     Name = "JumpHost Server"
#   }
# }

resource "aws_instance" "backend_srv" {
  ami             = data.aws_ami.latest_ubuntu.image_id
  instance_type   = var.backend_instance_type
  private_ip      = var.backend_instance_private_ip
  subnet_id       = aws_subnet.public_subnet.id
  key_name        = aws_key_pair.generated_key.key_name
  security_groups = [aws_security_group.backend_access_sg.id]

  tags = {
    Name = "Backend Server"
  }
}

resource "aws_instance" "hyperledger" {
  ami             = data.aws_ami.latest_ubuntu.image_id
  instance_type   = var.hyperledger_instance_type
  private_ip      = var.hyperledger_instance_private_ip
  subnet_id       = aws_subnet.private_subnet.id
  key_name        = aws_key_pair.generated_key.key_name
  security_groups = [aws_security_group.hyperledger_access_sg.id]

  tags = {
    Name = "Hyperledger Server"
  }
}

//============================

#
#
# resource "aws_launch_configuration" "web_srv_launch_conf" {
#   name_prefix     = "web_srv_ha_lc-"
#   image_id        = data.aws_ami.amzn_linux_latest.image_id
#   instance_type   = "t2.micro"
#   security_groups = [aws_security_group.allow_http_sg.id]
#   user_data       = file("user_data.sh")
#
#   lifecycle {
#     create_before_destroy = true
#   }
# }
#
# resource "aws_autoscaling_group" "web_srv_autoscaling_gr" {
#   name                 = "ASG-${aws_launch_configuration.web_srv_launch_conf.name}"
#   max_size             = 2
#   min_size             = 2
#   health_check_type    = "ELB"
#   min_elb_capacity     = 2
#   launch_configuration = aws_launch_configuration.web_srv_launch_conf.name
#   vpc_zone_identifier  = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
#   load_balancers       = [aws_elb.web_srv_elb.name]
#
#   dynamic "tag" {
#     for_each = {
#       Name   = "Web Server ASG"
#       Owner  = "YB"
#       TAGKEY = "TAGVALUE"
#     }
#     content {
#       key                 = tag.key
#       value               = tag.value
#       propagate_at_launch = true
#     }
#   }
#
#   lifecycle {
#     create_before_destroy = true
#   }
# }
#
# resource "aws_elb" "web_srv_elb" {
#   name = "web-srv-ha-elb"
#   availability_zones = [data.aws_availability_zones.available.names[0],
#   data.aws_availability_zones.available.names[1]]
#   security_groups = [aws_security_group.allow_http_sg.id]
#
#   listener {
#     instance_port     = 80
#     instance_protocol = "http"
#     lb_port           = 80
#     lb_protocol       = "http"
#   }
#
#   health_check {
#     healthy_threshold   = 2
#     unhealthy_threshold = 2
#     timeout             = 3
#     target              = "HTTP:80/"
#     interval            = 10
#   }
#
#   tags = {
#     Name = "web_srv_ha_elb"
#   }
# }
